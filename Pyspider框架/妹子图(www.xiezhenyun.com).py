#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Created on 2020-03-09 21:33:02
# Project: meizitu

from pyspider.libs.base_handler import *
import os

class Handler(BaseHandler):
    crawl_config = {
    }

    @every(minutes=24 * 60)
    def on_start(self):
        self.crawl('https://www.xiezhenyun.com/qita/ishow', callback=self.index_page,validate_cert=False)

    @config(age=10 * 24 * 60 * 60)
    def index_page(self, response):
        for each in response.doc('.meta-title').items():
            self.crawl(each.attr.href, callback=self.detail_page,validate_cert=False)
        next=response.doc('.next').attr.href
        self.crawl(next,callback=self.index_page,validate_cert=False)
              
        

    @config(priority=2)
    def detail_page(self, response):
        for each in response.doc('.aligncenter').items():
            self.crawl(each.attr.src, callback=self.detail_page2,validate_cert=False)
            
            
     
    def detail_page2(self, response):
        f=open(os.path.join('E:\\图片',response.url[-10:]),'wb')
        f.write(response.content)
        f.close()